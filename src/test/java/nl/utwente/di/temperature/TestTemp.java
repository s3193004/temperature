package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/*** Tests the Quoter */
public class TestTemp {
    @Test
    public void testTemp1() throws Exception{
        Converter converter = new Converter();
        double temp = converter.Fahrenheit(10.0);
        Assertions.assertEquals(10.0, temp, 50, "Temp in fahrenheit 10");

    }
}
